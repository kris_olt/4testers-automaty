from src.exercise_2_animal_requires_vaccination import check_if_animal_requires_vaccination
def test_cat_age_one_requires_vaccination():
    assert check_if_animal_requires_vaccination(1, "cat")
def test_cat_age_two_requires_vaccination():
    assert check_if_animal_requires_vaccination(2, "cat") == False
def test_cat_age_three_requires_vaccination():
    assert check_if_animal_requires_vaccination(3, "cat") == False
def test_cat_four_four_requires_vaccination():
    assert check_if_animal_requires_vaccination(4, "cat")
def test_dog_age_one_requires_vaccination():
    assert check_if_animal_requires_vaccination(1,"dog")
def test_dog_age_two_requires_vaccination():
    assert check_if_animal_requires_vaccination(2,"dog") == False
def test_dog_age_three_requires_vaccination():
    assert check_if_animal_requires_vaccination(3,"dog")
def test_dog_age_four_requires_vaccination():
    assert check_if_animal_requires_vaccination(4,"dog") == False