from src.exercise_4_employee_management import generate_employee_dictionary
import re
from src.exercise_4_employee_management import generate_list_of_employees_dictionaries_with_requested_length
from src.exercise_4_employee_management import return_all_employees_emails_with_more_than_ten_seniority_years
from src.exercise_4_employee_management import return_female_employees


def test_generate_employee_dictionary_has_correct_email_keys():
    test_employee = generate_employee_dictionary()
    assert "email" in test_employee.keys()
    assert "seniority_years" in test_employee.keys()
    assert "female" in test_employee.keys()


def test_generate_employee_dictionary_email_has_correct_value_pattern():
    test_employee = generate_employee_dictionary()
    email_pattern = re.compile(r"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}")
    assert email_pattern.match(test_employee["email"])


def test_generate_employee_dictionary_email_is_string():
    test_employee = generate_employee_dictionary()
    assert isinstance(test_employee["email"], str)


def test_generate_employee_dictionary_seniority_years_is_integer():
    test_employee = generate_employee_dictionary()
    assert isinstance(test_employee["seniority_years"], int)


def test_generate_employee_dictionary_female_is_boolean():
    test_employee = generate_employee_dictionary()
    assert isinstance(test_employee["female"], bool)


def test_list_of_employees_dictionaries_length():
    list_of_employees_dicts = generate_list_of_employees_dictionaries_with_requested_length(10)
    assert len(list_of_employees_dicts) == 10


def test_list_of_employees_emails_with_more_than_ten_seniority_years():
    employees_list = generate_list_of_employees_dictionaries_with_requested_length(50)
    senior_employees = return_all_employees_emails_with_more_than_ten_seniority_years(employees_list)
    for email in senior_employees:
        assert all(employee["seniority_years"] > 10 for employee in employees_list if employee["email"] == email)


def test_list_of_female_employees():
    employees_list = generate_list_of_employees_dictionaries_with_requested_length(10)
    female_employees = return_female_employees(employees_list)
    for employee in female_employees:
        assert employee["female"] == True
