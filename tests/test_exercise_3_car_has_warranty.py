from src.exercise_3_car_has_warranty import verify_if_car_has_a_warranty
def test_newer_car_with_low_milage_has_warranty():
    assert verify_if_car_has_a_warranty(2020, 30_000)
def test_newer_car_with_medium_milage_has_warranty():
    assert verify_if_car_has_a_warranty(2020, 70_000)
def test_older_car_with_low_milage_has_warranty():
    assert verify_if_car_has_a_warranty(2016, 30_000)
def test_older_car_with_high_milage_has_warranty():
    assert verify_if_car_has_a_warranty(2016, 120_000)