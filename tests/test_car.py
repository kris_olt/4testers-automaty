from src.car import Car


def test_car_brand():
    car = Car("Toyota", "Pink", 2022)
    assert car.brand == "Toyota"


def test_if_drive_method_increases_milage():
    car = Car("Toyota", "Red", 2022)
    car.drive(100)
    assert car.milage == 100


def test_if_get_age_returns_age_of_a_car():
    car = Car("Toyota", "Red", 2022)
    car_age = car.get_age()
    assert car_age == 2


def test_if_repaint_changes_car_color():
    car = Car("Toyota", "Red", 2022)
    car.repaint("Blue")
    assert car.color == "Blue"
