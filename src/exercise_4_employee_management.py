from random import randint, choice


def generate_employee_dictionary():
    random_name = ["john", "marika", "susan", "tony", "michael", "zoe", "tina", "jane", "alex", "george"]
    random_domain = ["gmx.com", "proton.com", "yahoo.com", "bing.net", "gmail.sk", "edu.com", "gov.com", "ebay.uk"]
    employee_dictionary = {
        "email": f"{choice(random_name)}{randint(0, 100)}@{choice(random_domain)}",
        "seniority_years": randint(0, 15),
        "female": choice([True, False])
    }
    return employee_dictionary


def generate_list_of_employees_dictionaries_with_requested_length(number_of_dictionaries):
    list_of_employees_dictionaries = []
    for _ in range(number_of_dictionaries):
        employee_dict = generate_employee_dictionary()
        list_of_employees_dictionaries.append(employee_dict)
    return list_of_employees_dictionaries


def return_all_employees_emails_with_more_than_ten_seniority_years(employee_list):
    list_of_employees_emails_with_more_than_ten_seniority_years = []
    for employee in employee_list:
        if int(employee["seniority_years"]) > 10:
            list_of_employees_emails_with_more_than_ten_seniority_years.append(employee["email"])
    return list_of_employees_emails_with_more_than_ten_seniority_years


def return_female_employees(employee_list):
    female_employees = []
    for employee in employee_list:
        if employee["female"]:
            female_employees.append(employee)
    return female_employees


if __name__ == '__main__':
    employees_list = generate_list_of_employees_dictionaries_with_requested_length(10)
    print(employees_list)
    emails = return_all_employees_emails_with_more_than_ten_seniority_years(employees_list)
    print(emails)
    female_employees = return_female_employees(employees_list)
    print(female_employees)
