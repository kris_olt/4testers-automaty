friends_name = "Grzegorz"
friends_age = 33
friends_pet_amount = 0
has_driving_license = True
friendship_years = 31.5

print("Name:", friends_name, sep="\t\t")
print("Age:", friends_age, sep="\n")
print("Pet amount:", friends_pet_amount, sep="\n")
print("Has driving license:", has_driving_license, sep="\n")
print("Friendship years:", friendship_years, sep="\n")

print("-----------------------")

print("Name:", friends_name,
      "Age:", friends_age,
      "Pet amount:", friends_pet_amount,
      sep="\n"
      )

name_surname = "Krzysztof Kowalski"
email = "kkowalski@gmail.com"
phone_number = "+431231234565"

# słabe
bio = "Name: " + name_surname + "\nEmail: " + email + "\nPhone: " + phone_number
print(bio)
# lepsze
print("Name:", name_surname, "\nEmail:", email, "\nPhone:", phone_number)
# najlepszy
bio_smarter = f"Name: {name_surname}\nEmail: {email}\nPhone: {phone_number}"
print(bio_smarter)
