animals = [
    {
        "kind": "dog",
        "age": 2,
        "male": True
    },
    {
        "kind": "cat",
        "age": 5,
        "male": False
    },
    {
        "kind": "fish",
        "age": 1,
        "male": True
    }
]
print(len(animals))
print(animals[0:])
last_animal = animals[-1]
last_animal_kind = last_animal["kind"]
print("the kind of last animal: ", last_animal_kind)
print("the kind of last animal: ", animals[2]["kind"])
animals.append(
    {
        "kind": "zebra",
        "age": 7,
        "male": False
    }
)
print(animals)
