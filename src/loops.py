import random


def print_ten_numbers():
    for i in range(10):
        print(i)


# def print_even_numbers_in_range():
#     for i in range(0, 21, 2):

def print_number_divisible_by_seven():
    for i in range(1, 31):
        if i % 7 == 0:
            print(i)


def print_random_numbers(number_of_listed_numbers):
    for i in range(number_of_listed_numbers):
        print(random.randint(1,10))

def print_list_with_ten_random_numbers_from_1000_to_5000():
    numbers = []
    for i in range(10):
        numbers.append(random.randint(1000, 1010))
    return numbers


def convert_celsius_to_fahrenheit(temp_in_celsius):
    return temp_in_celsius * 9 / 5 + 32


def convert_list_of_celsius_to_list_of_fahrenheit(celsius_list):
    fahrenheit_list = []
    for temperature in celsius_list:
        temperature_in_fahrenheit = convert_celsius_to_fahrenheit(temperature)
        fahrenheit_list.append(temperature_in_fahrenheit)
    return fahrenheit_list


def convert_list_of_celsius_to_list_of_fahrenheit_with_range(celsius_list):
    fahrenheit_list = []
    number_of_temperature = len(celsius_list)
    for i in range(number_of_temperature):
        temp_in_celsius = celsius_list[i]
        temperature_in_fahrenheit = convert_celsius_to_fahrenheit(temp_in_celsius)
        fahrenheit_list.append(temperature_in_fahrenheit)
    return fahrenheit_list


if __name__ == '__main__':
    print_ten_numbers()
    print_number_divisible_by_seven()
    print_random_numbers(9)
    print(print_list_with_ten_random_numbers_from_1000_to_5000())
    numbers = [1, 5, 123, 12, 222]
    list_of_temperatures_in_celsius = [10.3, 23.2, 15.8, 19.0, 14.0, 23.0, 25.0]
    print(map)
