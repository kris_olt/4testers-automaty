from datetime import date


class Car:

    def __init__(self, brand, color, registration_year):
        self.brand = brand
        self.color = color
        self.registration_year = registration_year
        self.milage = 0
        self.age = self.get_age()

    def drive(self, distance):
        self.milage += distance

    def get_age(self):
        age = date.today().year - self.registration_year
        return age

    def repaint(self, color):
        self.color = color
        return self.color


if __name__ == '__main__':
    car_audi = Car("Audi", "Black", 2020, )
    print(f"This car is an {car_audi.brand} in {car_audi.color} color from year {car_audi.registration_year}")
    car_audi.drive(1000)
    print("Current car milage is:", car_audi.milage)
    car_audi.drive(1000)
    print("Current car milage is:", car_audi.milage)
    car_audi.drive(5000)
    print("Current car milage is:", car_audi.milage)
    car_audi.get_age()
    print("Current car age is:", car_audi.age)
    car_audi.repaint("Yellow")
    print("Current car color is:", car_audi.color)
    car_audi.repaint("Red")
    print("Current car color is:", car_audi.color)
    car_tesla = Car("Tesla", "White", 2024)
    print(f"This car is a {car_tesla.brand} in {car_tesla.color} color from year {car_tesla.registration_year}")
    car_tesla.drive(5000)
    print("Current car milage is:", car_tesla.milage)
    car_tesla.drive(7000)
    print("Current car milage is:", car_tesla.milage)
    car_tesla.drive(9000)
    print("Current car milage is:", car_tesla.milage)
    car_tesla.get_age()
    print("Current car age is:", car_tesla.age)
    car_tesla.repaint("Grey")
    print("Current car color is:", car_tesla.color)
    car_tesla.repaint("Brown")
    print("Current car color is:", car_tesla.color)
    car_fiat = Car("Fiat", "Pink", 1999)
    print(f"This car is a {car_fiat.brand} in {car_fiat.color} color from year {car_fiat.registration_year}")
    print("Current car milage is:", car_fiat.milage)
    car_fiat.drive(7000)
    print("Current car milage is:", car_fiat.milage)
    car_fiat.drive(9000)
    print("Current car milage is:", car_fiat.milage)
    car_fiat.get_age()
    print("Current car age is:", car_fiat.age)
    car_fiat.repaint("Green")
    print("Current car color is:", car_fiat.color)
    car_fiat.repaint("Gold")
    print("Current car color is:", car_fiat.color)
