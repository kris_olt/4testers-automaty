def print_temperature_description(temperature_in_celcius):
    print(f"Temperature right now is {temperature_in_celcius} Celcius degrees")
    if temperature_in_celcius > 25:
        print("It's getting hot!")
    elif temperature_in_celcius > 0:
        print("It's quite OK.")
    else:
        print("It's getting cold :(")


def is_person_an_adult(age):
    if age >= 18:
        return True
    else:
        return False


if __name__ == '__main__':
    temperature_in_celcius = 26
    print_temperature_description(temperature_in_celcius)
    wiek_jarko = 12
    wiek_juźko = 13
    wiek_tosi = 18
    print(is_person_an_adult(wiek_tosi))
