def print_players_info(player_info):
    return f"The player {player_info['nick']} is of type {player_info['type']} and has {player_info['exp_points']} EXP"


if __name__ == '__main__':
    player_info = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000
    }

    print(print_players_info(player_info))
