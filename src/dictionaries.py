if __name__ == '__main__':
    animal = {
        "name": "Bax",
        "kind": "dog",
        "age": 12,
        "weight": 31.5,
        "is_male": True,
        "favourite_food": ["Dry food", "Banana", "Pancaces"]
    }

    print(animal["weight"])
    animal["weight"] = 8.8
    animal["likes_swimming"] = False
    del animal["is_male"]
    animal["favourite_food"].append("snacks")
    print(animal)
