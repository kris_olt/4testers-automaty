# Solution in one large function
# def check_if_animal_requires_vaccination(age, kind):
#     if (kind == "cat" and (age == 1 or age % 3 == 0)) or (kind == "dog" and (age == 1 or age % 2 == 0)):
#         return True
#     elif age <= 0:
#         raise ValueError("Incorrect age")
#     elif kind != "dog" and kind != "cat":
#         raise ValueError("Unsupported animal type")
#     else:
#         return False

# Better solution with few smaller functions
def validate_animal_data(age, kind):
    if age < 0:
        raise ValueError("Age must be greater than or equal to zero")
    elif kind not in ["dog", "cat"]:
        raise ValueError("Only dogs and cats are supported")


def is_animal_qualified_for_vaccination(age, kind):
    return (kind == "dog" and (age - 1) % 2 == 0) or (kind == "cat" and (age - 1) % 3 == 0)


def check_if_animal_requires_vaccination(age, kind):
    validate_animal_data(age, kind)
    return is_animal_qualified_for_vaccination(age, kind)


if __name__ == '__main__':
    print("Cat, 1:", check_if_animal_requires_vaccination(1, "cat"))
    print("Dog, 1:", check_if_animal_requires_vaccination(1, "dog"))
    print("Cat, 2:", check_if_animal_requires_vaccination(2, "cat"))
    print("Cat, 4:", check_if_animal_requires_vaccination(4, "cat"))
    print("Dog, 3:", check_if_animal_requires_vaccination(1, "dog"))
    print("Dog, 2:", check_if_animal_requires_vaccination(1, "dog"))
    print("Cat, -1:", check_if_animal_requires_vaccination(-1, "cat"))
    print("Dog, -1:", check_if_animal_requires_vaccination(-1, "dog"))
