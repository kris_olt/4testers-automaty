import random
from datetime import date


def generate_user(firstname, surnames, countries, domain):
    random_age = random.randint(5, 45)
    surname = random.choice(surnames)
    random_country = random.choice(countries)
    random_email = f"{firstname}.{surname}@{domain}"
    is_person_adult = random_age >= 18
    birth_year = date.today().year - random_age
    welcome_string = f"Hi! I'm {firstname} {surname}. I come from {random_country} and I was born in {birth_year}"

    data_dictionary = {
        "firstname": firstname,
        "lastname": surname,
        "country": random_country,
        "email": random_email,
        "age": random_age,
        "adult": is_person_adult,
        "birth_year": birth_year
    }

    return welcome_string, data_dictionary


def generate_female_data(female_fnames, surnames, countries, domain):
    people = []
    for _ in range(5):
        random_female_firstname = random.choice(female_fnames)
        welcome_string, female_data_dictionary = generate_user(random_female_firstname, surnames, countries, domain)
        print(welcome_string)
        print(female_data_dictionary)
        people.append(female_data_dictionary)
    return people


def generate_male_data(male_fnames, surnames, countries, domain):
    people = []
    for _ in range(5):
        random_male_firstname = random.choice(male_fnames)
        welcome_string, male_data_dictionary = generate_user(random_male_firstname, surnames, countries, domain)
        print(welcome_string)
        print(male_data_dictionary)
        people.append(male_data_dictionary)
    return people


def generate_user_data_dictionary():
    female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
    male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
    surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
    domain = "example.com"
    countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

    people = []
    people.extend(generate_female_data(female_fnames, surnames, countries, domain))
    people.extend(generate_male_data(male_fnames, surnames, countries, domain))

    return people


if __name__ == '__main__':
    generate_user_data_dictionary()
