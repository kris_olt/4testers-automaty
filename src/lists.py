def get_three_highest_numbers_in_the_list(list_of_numbers):
    sorted_list = sorted(list_of_numbers)
    return sorted_list[-3:]


if __name__ == '__main__':
    movies = ["Forest Gump", "Lord of the Rings", "Harry Potter", "Matrix", "John Wick"]
    print(len(movies))
    print(movies[0])
    last_movie_index = len(movies) - 1
    print(movies)

    movies.append(("Her"))
    movies.insert(2, "Interstellar")
    print(get_three_highest_numbers_in_the_list([1, 2, 3, 4, 5]))
