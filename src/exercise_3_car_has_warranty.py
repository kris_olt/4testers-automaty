from datetime import date


def validate_car_data(registration_year, mileage):
    if registration_year not in range(1900, date.today().year + 1):
        raise ValueError("Incorrect Car registration year")
    if not isinstance(mileage, int) or mileage < 0:
        raise ValueError("Mileage must be a non-negative integer")


def verify_if_car_has_a_warranty(registration_year, milage):
    validate_car_data(registration_year, milage)
    return (date.today().year - registration_year) < 5 and milage <= 60000


# Alternative solution
def car_has_warranty(registration_year, milage):
    age = date.today().year - registration_year
    return age <= 5 and milage <= 60_000


if __name__ == '__main__':
    print(verify_if_car_has_a_warranty(2020, 30_000))
    print(verify_if_car_has_a_warranty(2020, 70_000))
    print(verify_if_car_has_a_warranty(2016, 30_000))
    print(verify_if_car_has_a_warranty(2016, 12_000))
